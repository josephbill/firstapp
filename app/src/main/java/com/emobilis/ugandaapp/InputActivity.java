package com.emobilis.ugandaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class InputActivity extends AppCompatActivity {
    //declare the views
    EditText name,email;
    TextInputEditText bio,age;
    Button submit;
    //declare variables to store info in
    String user_name,user_email,user_bio;
    String user_age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);
        //declare the references to those views
        name = findViewById(R.id.etName);
        email = findViewById(R.id.etEmail);
        bio = findViewById(R.id.etBio);
        age = findViewById(R.id.etage);
        submit = findViewById(R.id.transit);

        //onClickListener
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitDetails();
            }
        });
    }

    //submit details
    public void submitDetails(){
        //get user input
        user_name = name.getText().toString().trim();
        user_email = email.getText().toString().trim();
        user_bio = bio.getText().toString().trim();
        user_age = age.getText().toString().trim();

        //validations
        if (user_name.isEmpty() && user_email.isEmpty() && user_bio.isEmpty() && user_age.isEmpty()){
            //notifications
            Toast.makeText(this,"One of the fields is empty",Toast.LENGTH_LONG).show();
        } else {
            //take user to next screen and share data
            Intent intent = new Intent(InputActivity.this,SharedInput_Activity.class);
            //to share data
            intent.putExtra("name",user_name);
            intent.putExtra("email",user_email);
            intent.putExtra("bio",user_bio);
            intent.putExtra("age",user_age);
            startActivity(intent);
        }


    }
}
