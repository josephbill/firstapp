package com.emobilis.ugandaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashScreenActivity extends AppCompatActivity {
    //declare timer variable
    int SPLASH_TIME_OUT = 3000; //3 seconds
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);


        //setting our handler
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //calling an intent
                Intent intent = new Intent(SplashScreenActivity.this,InputActivity.class);
                startActivity(intent);
            }
        }, SPLASH_TIME_OUT);


    }
}
