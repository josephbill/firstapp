package com.emobilis.ugandaapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class SharedInput_Activity extends AppCompatActivity {
    //declare the views
    TextView nameAge,bio,email;
    //variables to hold shared data
    String _name,_age,_bio,_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_input_);

        //find the reference to the views
        nameAge = findViewById(R.id.etNameAge);
        bio = findViewById(R.id.etBio);
        email = findViewById(R.id.etEmail);

        //get the shared data
        Intent intent = getIntent();
        _name = intent.getStringExtra("name");
        _age = intent.getStringExtra("age");
        _email = intent.getStringExtra("email");
        _bio = intent.getStringExtra("bio");

        //set the text to the containers
        nameAge.append(" Name: " + _name + " Age: " + _age);
        email.setText(_email);
        bio.setText(_bio);
    }

    public void exit(View v){
        //to terminate an app
        finishAffinity();
    }
}
